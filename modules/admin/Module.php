<?php

namespace app\modules\admin;
use Yii;
use yii\helpers\Url;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';
	public $layout = '/admin';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    /*public function beforeAction($action) {
       if(Yii::$app->user->can('admin'))
          return true;
       else
           return Yii::$app->response->redirect(Url::to('/'));
      }*/
}
