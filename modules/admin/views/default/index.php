<? 
use yii\helpers\Html;
use app\models\Cats;
?>
<div class="admin-default-index">
    <h1><?= Yii::t('app', 'Control') ?></h1>
	<p>
		<?= Html::a(Html::encode(Yii::t('app', 'Categories List')), '/cats/index') ?>
    </p>
    <p>
		<?= Html::a(Html::encode(Yii::t('app', 'Videos List')), '/videos/index') ?>
    </p>
    <p>
		<?= Html::a(Html::encode(Yii::t('app', 'Пользователи')), '/rbac/user/index') ?>
    </p>
	<p>
		<?= Html::a(Html::encode(Yii::t('app', 'Посещения')), '/visitors/index') ?>
    </p>
	<?
	/*$allCategories = (new \yii\db\Query())
						->select('*')
						->from('bgv_category')
						->all();
						echo count($allCategories);
		for($ic=0; $i<count($allCategories); $ic++){
					echo $allCategories[$ic]['name'] . '<br />';
		}*/
	?>
</div>
