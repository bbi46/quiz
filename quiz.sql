-- MySQL dump 10.13  Distrib 5.6.39-83.1, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cw64959_quiz
-- ------------------------------------------------------
-- Server version	5.6.39-83.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin','3',1601429408),('user','3',1601429408),('user','4',1601747717);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('/admin/*',2,NULL,NULL,NULL,1520339417,1520339417),('/category/ajax',2,NULL,NULL,NULL,1520339417,1520339417),('/category/countSummaryVideos',2,NULL,NULL,NULL,1500750808,1500750808),('/cats/create',2,NULL,NULL,NULL,1486984282,1486984282),('/coderway-events/view',2,NULL,NULL,NULL,1504602574,1504602574),('/coderway/delete',2,NULL,NULL,NULL,1504094623,1504094623),('/gii/*',2,NULL,NULL,NULL,1601534653,1601534653),('/gii/default/*',2,NULL,NULL,NULL,1601534653,1601534653),('/gii/default/action',2,NULL,NULL,NULL,1601534653,1601534653),('/gii/default/diff',2,NULL,NULL,NULL,1601534653,1601534653),('/gii/default/index',2,NULL,NULL,NULL,1601534653,1601534653),('/gii/default/preview',2,NULL,NULL,NULL,1601534653,1601534653),('/gii/default/view',2,NULL,NULL,NULL,1499702468,1499702468),('/learn-english-texts/create',2,NULL,NULL,NULL,1508628604,1508628604),('/learn-english-texts/delete',2,NULL,NULL,NULL,1508628611,1508628611),('/learn-english-texts/index',2,NULL,NULL,NULL,1508628597,1508628597),('/learn-english-texts/update',2,NULL,NULL,NULL,1508628608,1508628608),('/learn-english-texts/view',2,NULL,NULL,NULL,1508628600,1508628600),('/learnenglish/create',2,NULL,NULL,NULL,1499703926,1499703926),('/learnenglish/delete',2,NULL,NULL,NULL,1499704020,1499704020),('/learnenglish/index',2,NULL,NULL,NULL,1499703968,1499703968),('/learnenglish/sentences',2,NULL,NULL,NULL,1518596134,1518596134),('/learnenglish/update',2,NULL,NULL,NULL,1499703943,1499703943),('/learnenglish/view',2,NULL,NULL,NULL,1499704122,1499704122),('/news/*',2,NULL,NULL,NULL,1519244534,1519244534),('/news/create',2,NULL,NULL,NULL,1519244534,1519244534),('/news/delete',2,NULL,NULL,NULL,1519244534,1519244534),('/news/index',2,NULL,NULL,NULL,1519244534,1519244534),('/news/update',2,NULL,NULL,NULL,1519244534,1519244534),('/news/view',2,NULL,NULL,NULL,1519244534,1519244534),('/post/*',2,NULL,NULL,NULL,1486921385,1486921385),('/post/update',2,NULL,NULL,NULL,1487013541,1487013541),('/profile/index',2,NULL,NULL,NULL,1529782576,1529782576),('/rbac/*',2,NULL,NULL,NULL,1486921342,1486921342),('/rbac/assignment/*',2,NULL,NULL,NULL,1600850577,1600850577),('/rbac/assignment/assign',2,NULL,NULL,NULL,1600850577,1600850577),('/rbac/assignment/index',2,NULL,NULL,NULL,1600850577,1600850577),('/rbac/assignment/revoke',2,NULL,NULL,NULL,1600850577,1600850577),('/rbac/assignment/view',2,NULL,NULL,NULL,1600850577,1600850577),('/site/about',2,NULL,NULL,NULL,1530897011,1530897011),('/site/addvideo',2,NULL,NULL,NULL,1498645292,1498645292),('/site/callback',2,NULL,NULL,NULL,1500930770,1500930770),('/site/indextest',2,NULL,NULL,NULL,1499516263,1499516263),('/site/loadvideo',2,NULL,NULL,NULL,1529781845,1529781845),('/site/tanki',2,NULL,NULL,NULL,1579618012,1579618012),('/site/tree',2,NULL,NULL,NULL,1500004989,1500004989),('/treemanager/node/manage',2,NULL,NULL,NULL,1497974361,1497974361),('/treemanager/node/move',2,NULL,NULL,NULL,1498098550,1498098550),('/treemanager/node/remove',2,NULL,NULL,NULL,1499556443,1499556443),('/treemanager/node/save',2,NULL,NULL,NULL,1497974384,1497974384),('/video-questions/*',2,NULL,NULL,NULL,1572710789,1572710789),('/video-questions/create',2,NULL,NULL,NULL,1572710789,1572710789),('/video-questions/delete',2,NULL,NULL,NULL,1572710789,1572710789),('/video-questions/update',2,NULL,NULL,NULL,1572710789,1572710789),('/video-questions/view',2,NULL,NULL,NULL,1572710789,1572710789),('/videos/*',2,NULL,NULL,NULL,1569438332,1569438332),('/videos/bot',2,NULL,NULL,NULL,1572359610,1572359610),('/videos/create',2,NULL,NULL,NULL,1486982483,1486982483),('/videos/delete',2,NULL,NULL,NULL,1487014824,1487014824),('/videos/fbot',2,NULL,NULL,NULL,1573753342,1573753342),('/videos/index',2,NULL,NULL,NULL,1486983248,1486983248),('/videos/isrecommend',2,NULL,NULL,NULL,1551216866,1551216866),('/videos/isviewed',2,NULL,NULL,NULL,1547588275,1547588275),('/videos/music',2,NULL,NULL,NULL,1520266940,1520266940),('/videos/update',2,NULL,NULL,NULL,1487047487,1487047487),('/videos/update/*',2,NULL,NULL,NULL,1496192920,1496192920),('/videos/uview',2,NULL,NULL,NULL,1520282911,1520282911),('/videos/view',2,NULL,NULL,NULL,1486983790,1486983790),('/visitors/*',2,NULL,NULL,NULL,1516246692,1516246692),('/visitors/create',2,NULL,NULL,NULL,1516246683,1516246683),('/visitors/delete',2,NULL,NULL,NULL,1516246689,1516246689),('/visitors/index',2,NULL,NULL,NULL,1516246677,1516246677),('/visitors/statistic',2,NULL,NULL,NULL,1518551896,1518551896),('/visitors/update',2,NULL,NULL,NULL,1516246686,1516246686),('/visitors/view',2,NULL,NULL,NULL,1516246680,1516246680),('admin',1,'Главный администратор',NULL,NULL,1486922055,1569438387),('admin visits',2,NULL,NULL,NULL,1516246714,1516246714),('adminAccess',2,'общий доступ в админку',NULL,NULL,1486921599,1486921599),('adminNews',2,NULL,NULL,NULL,1519244456,1519244456),('adminVideoQuestions',2,'Allows full access to videos questions',NULL,NULL,1572710853,1572710853),('catsDelete',2,'Удаление категорий',NULL,NULL,1496258804,1496258804),('catsUpdate',2,'Изменение категорий',NULL,NULL,1494354156,1494354156),('changeVideo',2,'Разрешение обновлять или удалять видео',NULL,NULL,1487014788,1487014788),('createCats',2,'Разрешение создавать подкатегории',NULL,NULL,1486984326,1486984326),('createVideo',2,'разрешение создавать видео',NULL,NULL,1486982518,1486982518),('debug',2,NULL,NULL,NULL,1528169398,1528169398),('gii',2,'разрешение работать с модулем gii',NULL,NULL,1499702510,1499702510),('learn_english_texts_all_actions',2,'Разрешение работать со всеми действиями, которые относятся к сущности изучения английских текстов',NULL,NULL,1508628559,1508628629),('learnenglish',2,'Разрешение работать с данными таблицы для изучения английского языка по Петрову',NULL,NULL,1499704072,1499704072),('manageCoderway',2,'Управление записями Coderway',NULL,NULL,1504094584,1504094584),('manageCoderwayEvents',2,'Управление событиями \"Пути кодера\"',NULL,NULL,1504602539,1504602539),('manager',1,'Может добавлять посты',NULL,NULL,1486921939,1486921939),('postAccess',2,'доступ к чтению постов',NULL,NULL,1486921479,1486921479),('treemanager',2,'Разрешение управлять деревом категорий',NULL,NULL,1497974440,1497974440),('updateOwnPost',2,'возможность обновить только свою запись',NULL,NULL,1486928013,1486928113),('updateOwnVideo',2,'Разрешение редактировать исключительно свои добавленные видео',NULL,NULL,1487015969,1487017935),('updatePost',2,'обновить запись',NULL,NULL,1486927967,1486927967),('user',1,'зарегистрированный пользователь',NULL,NULL,1486921875,1486921875),('userAccess',2,'доступ к модулю RBAC. Только админы!!!',NULL,NULL,1486921709,1486921738),('videoAccess',2,'доступ к просмотру видео',NULL,NULL,1486979240,1486979240),('videosDelete',2,'Удаление записей с информацией о видео',NULL,NULL,1497022745,1497022745),('videosUpdate',2,'Изменение видео',NULL,NULL,1496192629,1496192629),('viewCateg',2,'Просмотр одной категории после создания',NULL,NULL,1494354368,1494354417);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('admin','/admin/*'),('admin','/category/ajax'),('manageCoderwayEvents','/coderway-events/view'),('manageCoderway','/coderway/delete'),('admin','/gii/*'),('gii','/gii/*'),('admin','/gii/default/*'),('gii','/gii/default/*'),('admin','/gii/default/action'),('gii','/gii/default/action'),('admin','/gii/default/diff'),('gii','/gii/default/diff'),('admin','/gii/default/index'),('gii','/gii/default/index'),('admin','/gii/default/preview'),('gii','/gii/default/preview'),('admin','/gii/default/view'),('gii','/gii/default/view'),('learn_english_texts_all_actions','/learn-english-texts/create'),('learn_english_texts_all_actions','/learn-english-texts/delete'),('learn_english_texts_all_actions','/learn-english-texts/index'),('learn_english_texts_all_actions','/learn-english-texts/update'),('learn_english_texts_all_actions','/learn-english-texts/view'),('learnenglish','/learnenglish/create'),('learnenglish','/learnenglish/delete'),('learnenglish','/learnenglish/index'),('admin','/learnenglish/sentences'),('learnenglish','/learnenglish/update'),('learnenglish','/learnenglish/view'),('adminNews','/news/*'),('adminNews','/news/create'),('adminNews','/news/delete'),('adminNews','/news/index'),('adminNews','/news/update'),('adminNews','/news/view'),('admin','/post/*'),('postAccess','/post/*'),('userAccess','/post/*'),('updatePost','/post/update'),('admin','/profile/index'),('admin','/rbac/*'),('userAccess','/rbac/*'),('userAccess','/rbac/assignment/*'),('userAccess','/rbac/assignment/assign'),('userAccess','/rbac/assignment/index'),('userAccess','/rbac/assignment/revoke'),('userAccess','/rbac/assignment/view'),('admin','/site/about'),('admin','/site/addvideo'),('admin','/site/callback'),('admin','/site/indextest'),('admin','/site/loadvideo'),('admin','/site/tanki'),('user','/site/tree'),('treemanager','/treemanager/node/manage'),('user','/treemanager/node/manage'),('treemanager','/treemanager/node/move'),('treemanager','/treemanager/node/remove'),('treemanager','/treemanager/node/save'),('adminVideoQuestions','/video-questions/*'),('adminVideoQuestions','/video-questions/create'),('adminVideoQuestions','/video-questions/delete'),('adminVideoQuestions','/video-questions/update'),('adminVideoQuestions','/video-questions/view'),('admin','/videos/*'),('admin','/videos/bot'),('createVideo','/videos/create'),('admin','/videos/delete'),('changeVideo','/videos/delete'),('videosDelete','/videos/delete'),('admin','/videos/fbot'),('admin','/videos/index'),('user','/videos/index'),('videoAccess','/videos/index'),('admin','/videos/isrecommend'),('admin','/videos/isviewed'),('admin','/videos/music'),('admin','/videos/update'),('changeVideo','/videos/update'),('videosUpdate','/videos/update'),('changeVideo','/videos/update/*'),('videosUpdate','/videos/update/*'),('videoAccess','/videos/uview'),('admin','/videos/view'),('changeVideo','/videos/view'),('videoAccess','/videos/view'),('admin visits','/visitors/*'),('admin visits','/visitors/create'),('admin visits','/visitors/delete'),('admin visits','/visitors/index'),('admin visits','/visitors/update'),('admin visits','/visitors/view'),('admin','adminAccess'),('manager','adminAccess'),('userAccess','adminAccess'),('admin','adminNews'),('admin','adminVideoQuestions'),('admin','catsDelete'),('admin','catsUpdate'),('admin','changeVideo'),('updateOwnVideo','changeVideo'),('admin','createCats'),('admin','debug'),('admin','gii'),('admin','learn_english_texts_all_actions'),('admin','learnenglish'),('admin','manageCoderway'),('admin','manageCoderwayEvents'),('admin','manager'),('user','postAccess'),('userAccess','postAccess'),('admin','treemanager'),('userAccess','updateOwnPost'),('admin','updateOwnVideo'),('user','updateOwnVideo'),('updateOwnPost','updatePost'),('userAccess','updatePost'),('manager','user'),('admin','videoAccess'),('changeVideo','videoAccess'),('admin','videosDelete'),('admin','videosUpdate'),('changeVideo','videosUpdate'),('admin','viewCateg');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `status` int(3) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'User202009300415','$2y$13$submJPDRhS.Wr1pajxfcEugwE2BAcRnJBRxvilE3qkXDtgUbsDVb2','','vank300828@gmail.com','DCD9HS_9qqRy8x3rKHvMi4VdxSTeQxlR',10,1601428576,1601428576,''),(2,'User5','$2y$13$n2FvDgn0CnHTAVkb7o2C.OZzS4oR6nmGDfdYLruKeENLbJNmk25mK','','vank300829@gmail.com','siN4NgefbUHwGRVa638SpYEuhAqrbgvS',10,1601429265,1601429265,''),(3,'User6','$2y$13$1UX3LgbiSQmpIevoMNEqweDIrbu2UkBCXjsFvLY2y3.iDkV8ijF0C','','vank300830@gmail.com','hxykWx4m-lPDA5JGoqmT60lqGB1Gull0',10,1601429408,1601429408,''),(4,'Frommler','$2y$13$2n6BLwd1cczlz9H.0lKwn.w48JXVchAVJ7646OFFZT0T5pxz/okgu','','albanec777@meta.ua','VQsBRDpnICd-RDJUd4YKd73cltM_eHEe',10,1601747717,1601747717,'');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-03 23:15:50
