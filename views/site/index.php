<?php
use yii\bootstrap\Button;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Приветствуем!</h1>

        <p class="lead">Выберите категорию</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Начинаем</a></p>
    </div>

    <div class="body-content">

        <div class="row1">
          <?= Button::widget([
              'label' => 'Науки',
              'options' => ['class' => 'quiz-categories-buttons']
            ])
          ?>
            <div class="col-lg-4">
                <button class="col-lg-btn"><h2>Сказки</h2></button>
            </div>
            <div class="col-lg-4">
                <button class="col-lg-btn"><h2>Программирование</h2></button>
            </div>
            <div class="col-lg-4">
                <button class="col-lg-btn"><h2>Фильмы</h2></button>
            </div>
        </div>
    </div>
</div>
