<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\BaseUrl;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use mdm\admin\components\Helper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="
	<?php echo Yii::$app->request->baseUrl;?>
	/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?= Yii::$app->request->baseUrl.'/web/css/admin.css' ?>">
	<?= Html::csrfMetaTags() ?>
	    
	<title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	
	
	
</head>
<header></header>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
	
    <?php
	
	$menuItems = [
            ['label' => 'Начало', 'url' => ['/admin/default/index']],
            ['label' => 'Записи', 'url' => ['/admin/post/index']],
			['label' => 'Пользователи', 'url' => ['/rbac/default/index']],
			['label' => 'Инфо', 'url' => ['/site/about']],
            ['label' => 'Связь', 'url' => ['/site/contact']],
			['label' => 'Регистрация', 'url' => ['/site/signup']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Войти', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выйти (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ];

$menuItems = Helper::filter($menuItems);

    NavBar::begin([
        'brandLabel' => 'Викторина',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => Helper::filter($menuItems),
    ]);
    NavBar::end();
    ?>
<script>
	$('.navbar').removeClass('navbar-fixed-top');
</script>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Админка <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
