<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Знание - Сила',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'W9AzhAQDVMuPKlGYlAifdQKGLUNtHrQ_',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
 'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
			'class' => 'app\components\UrlManager',
			'languages' => ['ru', 'en'],
            'rules' => [
				'<controller:cats>/<action:indexview>/<id:\d+>'=>'cats/indexview',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'/<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
				
				''=>'site/index',
                //'<action>'=>'site/<action>',
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action'
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
            ],
        'i18n' => [
         'translations' => [
           '*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@app/messages',
            'sourceLanguage' => 'ru-RU'
           ]
         ]
        ]
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module'
            ],
	'rbac' => [
            'class' => 'mdm\admin\Module',
			'controllerMap' => [
                 'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    /* 'userClassName' => 'app\models\User', */
                    'idField' => 'id',
                    'usernameField' => 'username',
                ],
            ],
			'layout' => 'left-menu',
            'mainLayout' => '@app/views/layouts/admin.php'
		],
        ],
    'params' => $params,
	'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/index',
            'site/login',
            'site/logout',
			'site/signup',
			'rbac/default/index',
			'admin'
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step;
		],
		]
    
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        //uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '77.111.247.189'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
         //uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '31.131.102.174', '77.111.247.189', '178.133.188.178'],
    ];
}

return $config;
